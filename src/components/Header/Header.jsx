import React from 'react'
import { AppBar, Typography }  from '@material-ui/core'

import useStyles from './styles'

const Header = () => {
    const classes = useStyles();

    return (
        <>
            <AppBar 
            position="fixed" 
            className={classes.appBar} 
            color="inherit"> 
                <Typography variant="h2" align="center">
                    Store
                </Typography>
            </AppBar>
        </>
    )
}

export default Header
