import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
    appBar: {
        boxShadow: 'none',
        border: '1px solid rgba(0, 0, 0, 0.12)',
        marginTop: '64px',
        height: '135px',
        justifyContent: 'center'
    }
}));