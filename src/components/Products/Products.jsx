import React from 'react'
import { Grid } from '@material-ui/core';
import Header from '../Header/Header'

import Product from './Product/product'
import useStyles from './styles'

// const products = [
//     { id: 1, name: 'Shoes', description: 'Street shoes.', price: '$20.000', image: 'https://www.backseries.com/wp-content/uploads/travis-scott-air-jordan-1-on-feet-CD4487-100-2.jpg' },
//     { id: 2, name: 'Macbook', description: 'Apple macbook.', price: '$250.000', image: 'https://www.ventasrosario.com.ar/wp-content/uploads/2020/04/1584552907_1553854.jpg'},
// ]

const Products = ({ products, onAddToCart }) => {
    const classes = useStyles() ;

    return (
        <main className={classes.content}>
            <Header />
            <div className={classes.toolbar} />
            <div className={classes.toolbar} />
            <div className={classes.toolbar} />
            <Grid container justify="center" spacing={3}>
                {products.map((product) => (
                    <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
                        <Product product={product} onAddToCart={onAddToCart}/>
                    </Grid>
                ))}
            </Grid>
        </main>
    )
    
}
export default Products;